# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.16](https://gitee.com/gitee-fe/osui/tree/master/compare/v1.2.15...v1.2.16) (2022-02-21)


### Bug Fixes

* 修复颜色变量导出报错, 修复ThemeProvider使用方式 ([f345300](https://gitee.com/gitee-fe/osui/tree/master/commits/f34530046011e9e39fbd1bdb9edb1e11856dfe96))





## [1.2.14](https://gitee.com/gitee-fe/osui/tree/master/compare/v1.2.13...v1.2.14) (2022-02-21)


### Bug Fixes

* 增加ThemeProvider组件 ([fb6ac5e](https://gitee.com/gitee-fe/osui/tree/master/commits/fb6ac5eb6d958b11d1b978c354a0c2ab8e2af91d))
